import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'categories.dart';
part 'trips.g.dart';

abstract class Trips implements Built<Trips, TripsBuilder> {
  Trips._();

  factory Trips([updates(TripsBuilder b)]) = _$Trips;

  @BuiltValueField(wireName: 'name')
  @nullable String get name;
  @BuiltValueField(wireName: 'departureStation')
  String get departureStation;
  @BuiltValueField(wireName: 'arrivalStation')
  String get arrivalStation;
  @BuiltValueField(wireName: 'runDepartureStation')
  String get runDepartureStation;
  @BuiltValueField(wireName: 'runArrivalStation')
  String get runArrivalStation;
  @BuiltValueField(wireName: 'departureTime')
  String get departureTime;
  @BuiltValueField(wireName: 'arrivalTime')
  String get arrivalTime;
  @BuiltValueField(wireName: 'trainNumber')
  String get trainNumber;
  @BuiltValueField(wireName: 'categories')
  BuiltList<Categories> get categories;
  @BuiltValueField(wireName: 'travelTimeInSeconds')
  String get travelTimeInSeconds;
  @BuiltValueField(wireName: 'firm')
  bool get firm;
  @BuiltValueField(wireName: 'numberForUrl')
  String get numberForUrl;
  

  static Serializer<Trips> get serializer => _$tripsSerializer;
}
