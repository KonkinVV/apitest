// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trips.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Trips> _$tripsSerializer = new _$TripsSerializer();

class _$TripsSerializer implements StructuredSerializer<Trips> {
  @override
  final Iterable<Type> types = const [Trips, _$Trips];
  @override
  final String wireName = 'Trips';

  @override
  Iterable<Object> serialize(Serializers serializers, Trips object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'departureStation',
      serializers.serialize(object.departureStation,
          specifiedType: const FullType(String)),
      'arrivalStation',
      serializers.serialize(object.arrivalStation,
          specifiedType: const FullType(String)),
      'runDepartureStation',
      serializers.serialize(object.runDepartureStation,
          specifiedType: const FullType(String)),
      'runArrivalStation',
      serializers.serialize(object.runArrivalStation,
          specifiedType: const FullType(String)),
      'departureTime',
      serializers.serialize(object.departureTime,
          specifiedType: const FullType(String)),
      'arrivalTime',
      serializers.serialize(object.arrivalTime,
          specifiedType: const FullType(String)),
      'trainNumber',
      serializers.serialize(object.trainNumber,
          specifiedType: const FullType(String)),
      'categories',
      serializers.serialize(object.categories,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Categories)])),
      'travelTimeInSeconds',
      serializers.serialize(object.travelTimeInSeconds,
          specifiedType: const FullType(String)),
      'firm',
      serializers.serialize(object.firm, specifiedType: const FullType(bool)),
      'numberForUrl',
      serializers.serialize(object.numberForUrl,
          specifiedType: const FullType(String)),
    ];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Trips deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TripsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'departureStation':
          result.departureStation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'arrivalStation':
          result.arrivalStation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'runDepartureStation':
          result.runDepartureStation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'runArrivalStation':
          result.runArrivalStation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'departureTime':
          result.departureTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'arrivalTime':
          result.arrivalTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'trainNumber':
          result.trainNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'categories':
          result.categories.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Categories)]))
              as BuiltList<Object>);
          break;
        case 'travelTimeInSeconds':
          result.travelTimeInSeconds = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'firm':
          result.firm = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'numberForUrl':
          result.numberForUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Trips extends Trips {
  @override
  final String name;
  @override
  final String departureStation;
  @override
  final String arrivalStation;
  @override
  final String runDepartureStation;
  @override
  final String runArrivalStation;
  @override
  final String departureTime;
  @override
  final String arrivalTime;
  @override
  final String trainNumber;
  @override
  final BuiltList<Categories> categories;
  @override
  final String travelTimeInSeconds;
  @override
  final bool firm;
  @override
  final String numberForUrl;

  factory _$Trips([void Function(TripsBuilder) updates]) =>
      (new TripsBuilder()..update(updates)).build();

  _$Trips._(
      {this.name,
      this.departureStation,
      this.arrivalStation,
      this.runDepartureStation,
      this.runArrivalStation,
      this.departureTime,
      this.arrivalTime,
      this.trainNumber,
      this.categories,
      this.travelTimeInSeconds,
      this.firm,
      this.numberForUrl})
      : super._() {
    if (departureStation == null) {
      throw new BuiltValueNullFieldError('Trips', 'departureStation');
    }
    if (arrivalStation == null) {
      throw new BuiltValueNullFieldError('Trips', 'arrivalStation');
    }
    if (runDepartureStation == null) {
      throw new BuiltValueNullFieldError('Trips', 'runDepartureStation');
    }
    if (runArrivalStation == null) {
      throw new BuiltValueNullFieldError('Trips', 'runArrivalStation');
    }
    if (departureTime == null) {
      throw new BuiltValueNullFieldError('Trips', 'departureTime');
    }
    if (arrivalTime == null) {
      throw new BuiltValueNullFieldError('Trips', 'arrivalTime');
    }
    if (trainNumber == null) {
      throw new BuiltValueNullFieldError('Trips', 'trainNumber');
    }
    if (categories == null) {
      throw new BuiltValueNullFieldError('Trips', 'categories');
    }
    if (travelTimeInSeconds == null) {
      throw new BuiltValueNullFieldError('Trips', 'travelTimeInSeconds');
    }
    if (firm == null) {
      throw new BuiltValueNullFieldError('Trips', 'firm');
    }
    if (numberForUrl == null) {
      throw new BuiltValueNullFieldError('Trips', 'numberForUrl');
    }
  }

  @override
  Trips rebuild(void Function(TripsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TripsBuilder toBuilder() => new TripsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Trips &&
        name == other.name &&
        departureStation == other.departureStation &&
        arrivalStation == other.arrivalStation &&
        runDepartureStation == other.runDepartureStation &&
        runArrivalStation == other.runArrivalStation &&
        departureTime == other.departureTime &&
        arrivalTime == other.arrivalTime &&
        trainNumber == other.trainNumber &&
        categories == other.categories &&
        travelTimeInSeconds == other.travelTimeInSeconds &&
        firm == other.firm &&
        numberForUrl == other.numberForUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, name.hashCode),
                                                departureStation.hashCode),
                                            arrivalStation.hashCode),
                                        runDepartureStation.hashCode),
                                    runArrivalStation.hashCode),
                                departureTime.hashCode),
                            arrivalTime.hashCode),
                        trainNumber.hashCode),
                    categories.hashCode),
                travelTimeInSeconds.hashCode),
            firm.hashCode),
        numberForUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Trips')
          ..add('name', name)
          ..add('departureStation', departureStation)
          ..add('arrivalStation', arrivalStation)
          ..add('runDepartureStation', runDepartureStation)
          ..add('runArrivalStation', runArrivalStation)
          ..add('departureTime', departureTime)
          ..add('arrivalTime', arrivalTime)
          ..add('trainNumber', trainNumber)
          ..add('categories', categories)
          ..add('travelTimeInSeconds', travelTimeInSeconds)
          ..add('firm', firm)
          ..add('numberForUrl', numberForUrl))
        .toString();
  }
}

class TripsBuilder implements Builder<Trips, TripsBuilder> {
  _$Trips _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _departureStation;
  String get departureStation => _$this._departureStation;
  set departureStation(String departureStation) =>
      _$this._departureStation = departureStation;

  String _arrivalStation;
  String get arrivalStation => _$this._arrivalStation;
  set arrivalStation(String arrivalStation) =>
      _$this._arrivalStation = arrivalStation;

  String _runDepartureStation;
  String get runDepartureStation => _$this._runDepartureStation;
  set runDepartureStation(String runDepartureStation) =>
      _$this._runDepartureStation = runDepartureStation;

  String _runArrivalStation;
  String get runArrivalStation => _$this._runArrivalStation;
  set runArrivalStation(String runArrivalStation) =>
      _$this._runArrivalStation = runArrivalStation;

  String _departureTime;
  String get departureTime => _$this._departureTime;
  set departureTime(String departureTime) =>
      _$this._departureTime = departureTime;

  String _arrivalTime;
  String get arrivalTime => _$this._arrivalTime;
  set arrivalTime(String arrivalTime) => _$this._arrivalTime = arrivalTime;

  String _trainNumber;
  String get trainNumber => _$this._trainNumber;
  set trainNumber(String trainNumber) => _$this._trainNumber = trainNumber;

  ListBuilder<Categories> _categories;
  ListBuilder<Categories> get categories =>
      _$this._categories ??= new ListBuilder<Categories>();
  set categories(ListBuilder<Categories> categories) =>
      _$this._categories = categories;

  String _travelTimeInSeconds;
  String get travelTimeInSeconds => _$this._travelTimeInSeconds;
  set travelTimeInSeconds(String travelTimeInSeconds) =>
      _$this._travelTimeInSeconds = travelTimeInSeconds;

  bool _firm;
  bool get firm => _$this._firm;
  set firm(bool firm) => _$this._firm = firm;

  String _numberForUrl;
  String get numberForUrl => _$this._numberForUrl;
  set numberForUrl(String numberForUrl) => _$this._numberForUrl = numberForUrl;

  TripsBuilder();

  TripsBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _departureStation = _$v.departureStation;
      _arrivalStation = _$v.arrivalStation;
      _runDepartureStation = _$v.runDepartureStation;
      _runArrivalStation = _$v.runArrivalStation;
      _departureTime = _$v.departureTime;
      _arrivalTime = _$v.arrivalTime;
      _trainNumber = _$v.trainNumber;
      _categories = _$v.categories?.toBuilder();
      _travelTimeInSeconds = _$v.travelTimeInSeconds;
      _firm = _$v.firm;
      _numberForUrl = _$v.numberForUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Trips other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Trips;
  }

  @override
  void update(void Function(TripsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Trips build() {
    _$Trips _$result;
    try {
      _$result = _$v ??
          new _$Trips._(
              name: name,
              departureStation: departureStation,
              arrivalStation: arrivalStation,
              runDepartureStation: runDepartureStation,
              runArrivalStation: runArrivalStation,
              departureTime: departureTime,
              arrivalTime: arrivalTime,
              trainNumber: trainNumber,
              categories: categories.build(),
              travelTimeInSeconds: travelTimeInSeconds,
              firm: firm,
              numberForUrl: numberForUrl);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'categories';
        categories.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Trips', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
