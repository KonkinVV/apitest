import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'categories.g.dart';

abstract class Categories implements Built<Categories, CategoriesBuilder> {
  Categories._();

  factory Categories([updates(CategoriesBuilder b)]) = _$Categories;

  @BuiltValueField(wireName: 'type')
  String get type;
  @BuiltValueField(wireName: 'price')
  int get price;
 
  static Serializer<Categories> get serializer => _$categoriesSerializer;
}