library api;

import 'dart:convert';
//import 'dart:html';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:vkapitest/models/serializers.dart';
import 'package:vkapitest/models/trips.dart';
part './search.dart';




_parseAndDecode(String response) {
  return jsonDecode(response);
}

_parseJson(String text) {
  return foundation.compute(_parseAndDecode, text);
}

class Api {
  static Dio dio;

  static void init([String authToken]) {
    BaseOptions options = new BaseOptions(
      baseUrl: "",
      connectTimeout: 10000,
      receiveTimeout: 10000,
      
      contentType: "application/json",
      headers: {"Accept-Language": "en","Accept": "application/json"},
    );
    dio = new Dio(options);    
    dio.interceptors.add(InterceptorsWrapper(onError: (DioError error) {
      if (error.response == null)
        print("Отсутствует соединение с сервером. Проверьте подключение");
      else if (error.response.statusCode == 502 || error.response.statusCode == 404 || error.response.statusCode == 500)
        print("Ошибка на сервере. Некорректный запрос");
      else if (error.response.statusCode >= 400) {       
      }
    }));

    (dio.transformer as DefaultTransformer).jsonDecodeCallback = _parseJson;
  }
}
