import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:vkapitest/models/trips.dart';
import 'api/api.dart';

Map<String, dynamic> stations;

class MyBody extends StatefulWidget {
  @override
  createState() => new MyBodyState();
}

class MyBodyState extends State<MyBody> {
  List<Trips> _array = [];
  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, i) {
        print('num $i : нечетное = ${i.isOdd}');
        return ListTile(
          title: GestureDetector(
            child: Text(_array[i].departureStation),
            onTap: () {
              getData();
            },
          ),
        );
      },
      separatorBuilder: (context, i) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Divider(
          height: 5,
          color: Colors.black,
        ),
      ),
      itemCount: _array.length,
    );
  }

  getData() async {
    Api.init();
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/tutu_routes.json");
    stations = json.decode(data);
    _array.addAll(await TripsApi.search());
    print(stations[_array[0].departureStation]);
    setState(() {});
  }
}

void main() {
  return runApp(
    new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Scaffold(
        body: new Center(
          child: new MyBody(),
        ),
      ),
    ),
  );
}
